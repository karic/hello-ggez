use ggez::event::{self, EventHandler};
use ggez::graphics::{self, DrawParam};
use ggez::{Context, ContextBuilder, GameResult};

use glam::Vec2;

fn main() {
    let (mut ctx, event_loop) = ContextBuilder::new("game_id", "author")
        .build()
        .expect("Could not create ggez context!");

    let my_game = MyGame::new(&mut ctx).unwrap();

    event::run(ctx, event_loop, my_game)
}

struct MyGame {
    image1: graphics::Image,
}

impl MyGame {
    pub fn new(ctx: &mut Context) -> GameResult<MyGame> {
        let image1 = graphics::Image::new(ctx, "/water.png")?;
        let g = MyGame { image1 };
        Ok(g)
    }
}

impl EventHandler for MyGame {
    fn update(&mut self, _ctx: &mut Context) -> GameResult<()> {
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        let my_dest = Vec2::new(20.0, 10.0);

        graphics::clear(ctx, graphics::Color::WHITE);
        graphics::draw(ctx, &self.image1, DrawParam::default().dest(my_dest))?;
        graphics::present(ctx)
    }
}
