# Hello ggez

This is a step by step tutorial for making a game in Rust with [ggez](https://ggez.rs/).

The explanation is at [cosmin.hume.ro/ggez-tutorial](https://cosmin.hume.ro/ggez-tutorial/).

## Credits

The images are from [the Liberated Pixel Cup (LPC) package](https://opengameart.org/content/liberated-pixel-cup-lpc-base-assets-sprites-map-tiles).
